//+------------------------------------------------------------------+
//|                                          TelegramSignalAlert.mq4 |
//|                                              Olorunishola Falana |
//|                                         sholafalana777@gmail.com |
//+------------------------------------------------------------------+
#property copyright "Olorunishola Falana"
#property link      "sholafalana777@gmail.com"
#property version   "1.00"
#property strict
#include <Telegram.mqh>

//--- input parameters
input string InpChannelName="@hoanggiathe";//Channel Name
input string InpToken="936355269:AAFQHsmEgmZAtqYmRsu_fxTg_BojzGl7uHY"; //TEREKI_MT4
extern string mySigalname = "ROBOT TYPE";

//--- global variables
CCustomBot bot;
int macd_handle;
datetime time_signal=0;
bool checked;

bool AlertonTelegram = true;
bool MobileNotification = false;
bool EmailNotification = false;

extern bool alert_orderclosed = true;
extern bool alert_closepartial = true;
extern bool alert_ordermodified = true;
extern bool alert_Pendingfilled = true;
extern bool alert_new_pending = true;
extern bool alert_new_order = true;
extern bool alert_pending_deleted = true;

class OrderStore
  {
public:
   double            lot;
   double            stop_loss;
   double            take_profit;
   int               order_ticket;
   OrderStore() {
      lot = 0.0;
      stop_loss = 0.0;
      take_profit = 0.0;
      order_ticket = 0;
   }
   OrderStore(int id, double l, double sl, double tp) {
      order_ticket = id;
      lot = l;
      stop_loss = sl;
      take_profit = tp;
   }
  };


int totalord,totalpnd,totalopn;
OrderStore lastTotalOrder[];
//+------------------------------------------------------------------+
//| expert initialization function                                   |
//+------------------------------------------------------------------+
int init()
  {
//----




//----
   return(0);
  }
//+------------------------------------------------------------------+
//| expert deinitialization function                                 |
//+------------------------------------------------------------------+
int deinit()
  {
//----

//----
   return(0);
  }
void OnChartEvent(const int id,const long &lparam,const double &dparam,const string &sparam)
  {
   if(id==CHARTEVENT_KEYDOWN &&
      lparam=='Q')
     {

      bot.SendMessage(InpChannelName,"ee\nAt:100\nDDDD");
     }
  }

//+------------------------------------------------------------------+
//| expert start function                                            |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int start()
  {
   time_signal=0;

   bot.Token(InpToken);
   if(!checked)
     {
      if(StringLen(InpChannelName)==0)
        {
         Print("Error: Channel name is empty");
         Sleep(10000);
         return (0);
        }

      int result=bot.GetMe();
      if(result==0)
        {
         Print("Bot name: ",bot.Name());
         checked=true;
        }
      else
        {
         Print("Error: ",GetErrorDescription(result));
         Sleep(10000);
         return(0);
        }
     }


   string msg, msgbuy, msgsell, msgclos,msgfilled,msgdel,action1,action2,action3,msgpend,action4;


   int tmp = OrdersTotal();

   if(tmp < totalord)
     {
      // last closed order fixed
      int last_trade= HistoryTotal();
      if(last_trade>0)
        {
         if(OrderSelect(last_trade-1,SELECT_BY_POS,MODE_HISTORY)==true)
           {
            int digit = Digit(OrderSymbol());
            if((OrderType()==OP_BUY) || (OrderType()==OP_SELL))
              {
               string action1 = "ORDER CLOSED";
               msgclos =StringFormat("Ticket ID: %s\nName: %s\nSymbol: %s\nPrice: %s\nStrategy: %s\nReward: %s\nTime: %s\nAction: %s",
                                     IntegerToString(OrderTicket()),
                                     mySigalname,
                                     OrderSymbol(),
                                     DoubleToString(OrderClosePrice(), digit),
                                     OrderComment(),
                                     DoubleToString(OrderProfit() + OrderCommission() + OrderSwap()),
                                     TimeToString(TimeCurrent()),
                                     action1);

               //msg= StringConcatenate(OrderTypeString(OrderType())  ," Order closed : "," ", OrderSymbol()," "   ,OrderLots() , " Lot profit ",OrderProfit());
               if(alert_orderclosed)
                 {
                  if(MobileNotification)
                    {
                     SendNotification(msgclos);
                    }
                  if(EmailNotification)
                    {
                     SendMail("Order changes Notification",msgclos);
                    }
                  if(AlertonTelegram)
                    {
                     bot.SendMessage(InpChannelName,msgclos);
                    }
                 }

               totalord = tmp;
               return(0);
              }
           }
        }
     }


// send new order alert
   int lastOpenTime = 0;
   int tmp_pnd,temp_opn;
   int ord_type;
   for(int i = (OrdersTotal()-1); i >= 0; i --)
     {
      OrderSelect(i, SELECT_BY_POS, MODE_TRADES);
      int curOpenTime = OrderOpenTime();
      int digit = Digit(OrderSymbol());
      if(curOpenTime > lastOpenTime)
        {
         lastOpenTime = curOpenTime;
         ord_type = OrderType();
         msg=StringFormat("TicketID: %s\nName: %s\nSymbol: %s\nType: %s\nLots: %s\nEntry: %s\nTakeProfit: %s\nStopLoss: %s\nRisk: %s\nStrategy: %s\nTime: %s\nAction: ORDER OPENED",
                          IntegerToString(OrderTicket()),
                          mySigalname,
                          OrderSymbol(),
                          OrderTypeString(OrderType()),
                          DoubleToString(OrderLots(), 2),
                          DoubleToString(OrderOpenPrice(),digit),
                          DoubleToString(OrderTakeProfit(), digit),
                          DoubleToString(OrderStopLoss(), digit),
                          DoubleToString(MathAbs(OrderOpenPrice() - OrderStopLoss()), digit),
                          OrderComment(),
                          TimeToString(TimeCurrent()));
         //  msg  = StringConcatenate("New ", OrderTypeString(ord_type) ," Order   " , OrderSymbol()," "   ,OrderLots() , " Lot  @", OrderOpenPrice());
        }
      // if(cmd!=OP_BUY && cmd!=OP_SELL)
      if((OrderType()==OP_BUY) || (OrderType()==OP_SELL))
        {
         temp_opn=temp_opn + 1;
        }
      else
        {
         tmp_pnd =tmp_pnd +1 ;
        }
     }
   if(tmp > totalord)
     {
      if((ord_type==OP_BUY))
        {

         if(alert_new_order)
           {
            if(MobileNotification)
              {
               SendNotification(msg);
              }
            if(EmailNotification)
              {
               SendMail("Order changes Notification",msg);
              }
            if(AlertonTelegram)
              {
               bot.SendMessage(InpChannelName,msg);
              }
           }
        }
      if((ord_type==OP_SELL))
        {
         if(alert_new_order)
           {
            if(MobileNotification)
              {
               SendNotification(msg);
              }
            if(EmailNotification)
              {
               SendMail("Order changes Notification",msg);
              }
            if(AlertonTelegram)
              {
               bot.SendMessage(InpChannelName,msg);
              }
           }

        }

     }

   int digit = Digit(OrderSymbol());
//-----------------------
   if(tmp_pnd != totalpnd)
     {
      //pending filled or deleted
      if(tmp_pnd < totalpnd)
        {
         if(totalopn < temp_opn)
           {
            if(alert_Pendingfilled)
              {
               //action2 = "PENDING OPENED";
               //msgfilled =StringFormat("TicketID: %s\nName: %s \nSymbol: %s\nStrategy: %s\Time: %s\nAction: %s",
               //                          IntegerToString(OrderTicket()),
               //                          mySigalname,
               //                          OrderSymbol(),
               //                          OrderComment(),
               //                          TimeToString(TimeCurrent()),
               //                          action2);
               msg=StringFormat("TicketID: %s\nName: %s\nSymbol: %s\nType: %s\nLots: %s\nEntry: %s\nTakeProfit: %s\nStopLoss: %s\nRisk: %s\nStrategy: %s\nAction: %s",
                                IntegerToString(OrderTicket()),
                                mySigalname,
                                OrderSymbol(),
                                OrderTypeString(OrderType()),
                                DoubleToString(OrderLots(), 2),
                                DoubleToString(OrderOpenPrice(),digit),
                                DoubleToString(OrderTakeProfit(), digit),
                                DoubleToString(OrderStopLoss(), digit),
                                DoubleToString(MathAbs(OrderOpenPrice() - OrderStopLoss()), digit),
                                OrderComment(),
                                "PENDING OPENED"
                               );
               if(MobileNotification)
                 {
                  SendNotification(msg);
                 }
               if(EmailNotification)
                 {
                  SendMail("Order changes Notification",msg);
                 }
               if(AlertonTelegram)
                 {
                  bot.SendMessage(InpChannelName,msg);
                 }
              }
           }
         else
           {
            action3 = "PENDING CLOSED ";
            msgdel =StringFormat("TicketID: %s\nName: %s\nSymbol: %s\nAction: %s",IntegerToString(OrderTicket()),mySigalname,OrderSymbol(),action3);
           }
         if(alert_pending_deleted)
           {
            if(MobileNotification)
              {
               SendNotification(msgdel);
              }
            if(EmailNotification)
              {
               SendMail("Order changes Notification",msgdel);
              }
            if(AlertonTelegram)
              {
               bot.SendMessage(InpChannelName,msgdel);
              }
           }
        }

      // new pending placed
      if(tmp_pnd > totalpnd)
        {
         if(alert_new_pending)
           {
            msg=StringFormat("TicketID: %s\nName: %s\nSymbol: %s\nType: %s\nLots: %s\nEntry: %s\nTakeProfit: %s\nStopLoss: %s\nRisk: %s\nStrategy: %s\nAction: %s",
                             IntegerToString(OrderTicket()),
                             mySigalname,
                             OrderSymbol(),
                             OrderTypeString(OrderType()),
                             DoubleToString(OrderLots(), 2),
                             DoubleToString(OrderOpenPrice(),digit),
                             DoubleToString(OrderTakeProfit(), digit),
                             DoubleToString(OrderStopLoss(), digit),
                             DoubleToString(MathAbs(OrderOpenPrice() - OrderStopLoss()), digit),
                             OrderComment(),
                             "PENDING NEW"
                            );
            //action4 = "New Pending order ";
            // msgpend =StringFormat("Name: %s\nSymbol: %s\nAction: %s",mySigalname,OrderSymbol(),action4);
            if(MobileNotification)
              {
               SendNotification(msg);
              }
            if(EmailNotification)
              {
               SendMail("Order changes Notification",msg);
              }
            if(AlertonTelegram)
              {
               bot.SendMessage(InpChannelName,msg);
              }
           }

        }
     }

      //order modified
      for(int i = (totalord-1); i >= 0; i --)
      {
         OrderSelect(lastTotalOrder[i].order_ticket, SELECT_BY_TICKET, MODE_TRADES);
         if (alert_closepartial && lastTotalOrder[i].lot != OrderLots()) { //close partial
            double closePrice = ClosePartialPrice(OrderSymbol(), OrderType());
            msg=StringFormat("TicketID: %s\nName: %s\nSymbol: %s\nType: %s\nLots: %s\nClosed Lots: %s\nAt Price: %s\nAction: %s",
                                IntegerToString(OrderTicket()),
                                mySigalname,
                                OrderSymbol(),
                                OrderTypeString(OrderType()),
                                DoubleToString(OrderLots(), 2),
                                DoubleToString(lastTotalOrder[i].lot - OrderLots(),digit),
                                DoubleToString(closePrice, digit),
                                "CLOSE PARTITAL"
                               );
               //action4 = "New Pending order ";
               // msgpend =StringFormat("Name: %s\nSymbol: %s\nAction: %s",mySigalname,OrderSymbol(),action4);
               if(MobileNotification)
                 {
                  SendNotification(msg);
                 }
               if(EmailNotification)
                 {
                  SendMail("Order changes Notification",msg);
                 }
               if(AlertonTelegram)
                 {
                  bot.SendMessage(InpChannelName,msg);
                 }
         }
         if (alert_ordermodified && (lastTotalOrder[i].stop_loss != OrderStopLoss() || lastTotalOrder[i].take_profit != OrderTakeProfit())) { //modified order
            msg=StringFormat("TicketID: %s\nName: %s\nSymbol: %s\nType: %s\nLots: %s\nEntry: %s\nTakeProfit: %s\nStopLoss: %s\nRisk: %s\nStrategy: %s\nAction: %s",
                             IntegerToString(OrderTicket()),
                             mySigalname,
                             OrderSymbol(),
                             OrderTypeString(OrderType()),
                             DoubleToString(OrderLots(), 2),
                             DoubleToString(OrderOpenPrice(),digit),
                             DoubleToString(OrderTakeProfit(), digit),
                             DoubleToString(OrderStopLoss(), digit),
                             DoubleToString(MathAbs(OrderOpenPrice() - OrderStopLoss()), digit),
                             OrderComment(),
                             "ORDER MODIFIED"
                            );
            if(MobileNotification)
              {
               SendNotification(msg);
              }
            if(EmailNotification)
              {
               SendMail("Order changes Notification",msg);
              }
            if(AlertonTelegram)
              {
               bot.SendMessage(InpChannelName,msg);
              }
         }
      }

//-------------------------

   totalpnd=tmp_pnd;
   totalopn=temp_opn;
   totalord = tmp;
   ArrayResize(lastTotalOrder, OrdersTotal());
   for(int i = (OrdersTotal()-1); i >= 0; i --)
   {
      OrderSelect(i, SELECT_BY_POS, MODE_TRADES);
      lastTotalOrder[i] = OrderStore(OrderTicket(), OrderLots(), OrderStopLoss(), OrderTakeProfit());
   }
//  }  // end of total ord change

   return(0);
  }

//+------------------------------------------------------------------+
const string _orderTypesString[]= {"BUY","SELL","BUY_LIMIT","SELL_LIMIT","BUY_STOP","SELL_STOP"};
int Digit(string symbol)
  {
   return MarketInfo(symbol, MODE_DIGITS);
  }
string OrderTypeString(int type)
  {
   return _orderTypesString[type];
  }
double ClosePartialPrice(string symbol, int type) {
   string s = _orderTypesString[type];
   if (StringFind(s, "BUY") >= 0)
      return MarketInfo(symbol,MODE_BID) ;
   else
      return MarketInfo(symbol,MODE_ASK) ;
}
//+------------------------------------------------------------------+
